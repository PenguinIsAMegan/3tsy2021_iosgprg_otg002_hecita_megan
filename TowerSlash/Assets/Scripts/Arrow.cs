﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public string colour, orientation, gotOrientation;
    private bool isFlippedY, isFlippedX;

    // Start is called before the first frame update
    void Start()
    {
        isFlippedY = this.GetComponent<SpriteRenderer>().flipY;
        isFlippedX = this.GetComponent<SpriteRenderer>().flipX;
        getArrowInfo();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    // Stores Info
    public void setInfo(string givenColour, string givenOrientation)
    {
        this.colour = givenColour;
        this.gotOrientation = givenOrientation;   
    }

    // Gets Info of generated arrow
    public void getArrowInfo()
    {
        if (this.gotOrientation == "Vertical")
        {
            if (isFlippedY == false)
            {
                this.orientation = "Up";
            }

            else if (isFlippedY == true)
            {
                this.orientation = "Down";
            }
        }

        else if (this.gotOrientation == "Horizontal")
        {
            if (isFlippedX == false)
            {
                this.orientation = "Right";
            }

            else if (isFlippedX == true)
            {
                this.orientation = "Left";
            }
        }
    }
}
