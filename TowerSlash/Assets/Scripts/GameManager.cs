﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    private Enemy closestEnemy;
    private Arrow closestArrow;
    private Boss closestBoss;

    private List<Enemy> enemyList = new List<Enemy>();
    private List<Arrow> arrowList = new List<Arrow>();
    private List<Boss> bossList = new List<Boss>();

    public static GameManager Instance;
    public Player player;

    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Look and return closest enemy
    public Enemy findClosestEnemy()
    {
        enemyList = Spawner.Instance.getEnemyList();

        if (enemyList.Count > 0)
        {
            float distanceToClosestEnemy = Mathf.Infinity;

            foreach (Enemy currentEnemy in enemyList)
            {
                float distanceToEnemy = (currentEnemy.transform.position - player.transform.position).sqrMagnitude;

                if (distanceToEnemy < distanceToClosestEnemy)
                {
                    distanceToClosestEnemy = distanceToEnemy;
                    closestEnemy = currentEnemy;
                }
            }
        }

        return closestEnemy;
    }

    // Look and return closest arrow 
    public Arrow findClosestArrow(string enemy)
    { 
        if (enemy == "enemy")
        {
            if (closestEnemy != null)
            {
                arrowList = closestEnemy.getArrowList();

                if (arrowList.Count > 0)
                {
                    float distanceToClosestTarget = Mathf.Infinity;

                    foreach (Arrow currentTarget in arrowList)
                    {
                        float distanceToTarget = (currentTarget.transform.position - player.transform.position).sqrMagnitude;

                        if (distanceToTarget < distanceToClosestTarget)
                        {
                            distanceToClosestTarget = distanceToTarget;
                            closestArrow = currentTarget;
                        }
                    }
                }
            }
        }

        else
        {
            if (closestBoss != null)
            {
                arrowList = closestBoss.getArrowList();
                if (arrowList.Count > 0)
                {
                    float distanceToClosestTarget = Mathf.Infinity;

                    foreach (Arrow currentTarget in arrowList)
                    {
                        float distanceToTarget = (currentTarget.transform.position - player.transform.position).sqrMagnitude;

                        if (distanceToTarget < distanceToClosestTarget)
                        {
                            distanceToClosestTarget = distanceToTarget;
                            closestArrow = currentTarget;
                        }
                    }
                }
            }
        }
       
        return closestArrow;
    }

    // Remove defeated enemy
    public void removeEnemy(Enemy enemy)
    {
        enemyList.Remove(enemy);
        Destroy(enemy.gameObject);
    }

    // Remove destroyed arrow
    public void removeArrow(Arrow arrow)
    {
        arrowList.Remove(arrow);
        Destroy(arrow.gameObject);
    }

    // Look and return closest boss
    public Boss findClosestBoss()
    {
        bossList = Spawner.Instance.getBossList();

        if (bossList.Count > 0)
        {
            float distanceToClosestBoss = Mathf.Infinity;

            foreach (Boss currentBoss in bossList)
            {
                float distanceToBoss = (currentBoss.transform.position - player.transform.position).sqrMagnitude;

                if (distanceToBoss < distanceToClosestBoss)
                {
                    distanceToClosestBoss = distanceToBoss;
                    closestBoss = currentBoss;
                }
            }
        }

        return closestBoss;
    }

    // Remove defeated boss
    public void removeBoss(Boss boss)
    {
        bossList.Remove(boss);
        Destroy(boss.gameObject);
    }
}
