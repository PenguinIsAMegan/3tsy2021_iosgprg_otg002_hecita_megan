﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private List<Arrow> arrowList = new List<Arrow>();
    private bool isDone = false;
    private int gold, score;
    private GameObject newArrow;

    public int difficulty;
    public Player player;
    public Vector3 rangePoint;

    
    

    // Start is called before the first frame update
    void Start()
    {
        // Gets player and rangePoint
        rangePoint = Spawner.Instance.getRangePoint();
        player = GameObject.FindObjectOfType<Player>();

        // Randomise if it'll generate normal arrows or rotating
        int num = Random.Range(0, 100);

        if(num > 20)
        {
            StartCoroutine(spawnArrows());
        }

        else
        {
            StartCoroutine(spawnRotatingArrows());
        }
    }

    // Update is called once per frame
    void Update()
    {
        // For rotating arrows, if Player is in range, will spawn final arrow
        if (isDone == true)
        {
            newArrow = Spawner.Instance.generateArrow(0);
            arrowList.Add(newArrow.GetComponent<Arrow>());
            isDone = false;
        }

        // When player missed enemy, enemy will destroy self instead
        destroyEnemy();
    }

    // Gets difficulty
    public void getDifficulty(int level)
    {
        this.difficulty = level;

        if (difficulty > 3)
        {
            difficulty = 3;
        }
    }

    // Spawning regular arrows
    IEnumerator spawnArrows()
    {
        for (int i = 0; i < this.difficulty; i++)
        {
            GameObject newArrow = Spawner.Instance.generateArrow(i);
            arrowList.Add(newArrow.GetComponent<Arrow>());
            yield return new WaitForSeconds(1);
        }
    }

    // Spawning rotating arrows
    IEnumerator spawnRotatingArrows()
    {
        

        while (isInRange() == false)
        {
            newArrow = Spawner.Instance.generateArrow(0);
            yield return new WaitForSeconds(0.5f);
            Destroy(newArrow.gameObject);
        }
        
        isDone = true;
    }

    // Checks for Player position
    private bool isInRange()
    {
        if(player.transform.position.y <= rangePoint.y)
        {
            return false;
        }

        else
        {
            return true;
        }
    }

    //If Player swiped right, arrow will be removed from list
    public void removeArrow(Arrow currentArrow)
    {
        arrowList.Remove(currentArrow.GetComponent<Arrow>());
    }

    // Accessor for arrow count
    public int getArrowCount()
    {
        return arrowList.Count;
    }

    // If player missed, will destroy remaining arrows
    public void destroyRemainingArrows()
    {
        if (this.arrowList.Count > 0)
        {
            for (int i = 0; i < this.arrowList.Count; i++)
            {
                Destroy(this.arrowList[i].gameObject);
            }
        }
    }

    // Gives rewards
    public int giveGold()
    {
        return gold = 10 * this.difficulty;
    }

    public int giveScore()
    {
        return score = 100 * this.difficulty;
    }

    // When player missed enemy, enemy will destroy self instead
    public void destroyEnemy()
    {
        if (player.transform.position.y > this.transform.position.y)
        {
            destroyRemainingArrows();
            Spawner.Instance.newSpawnPoint();
            Spawner.Instance.spawnEnemy();
            GameManager.Instance.removeEnemy(this.GetComponent<Enemy>());
            Destroy(this.gameObject);
        }
    }

    // Accessor for list of arrows
    public List<Arrow> getArrowList()
    {
        return arrowList;
    }
}
