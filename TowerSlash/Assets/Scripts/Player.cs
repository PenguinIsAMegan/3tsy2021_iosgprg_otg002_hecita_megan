﻿using System.Collections;
using System.Collections.Generic;
using System.Security;
using TMPro;
using Unity.Profiling;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class Player : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] Sprite wrong, hurt, punch, run, shoot;
    [SerializeField] float playerSpeed;
    [SerializeField] float playerDashSpeed;
    [SerializeField] GameObject skill;
    [SerializeField] Transform skillSpawnPoint;
    [SerializeField] Transform bossRange;

    private Vector2 startTouchPos, endTouchPos;
    private float swipeLimit = 10f, dashTimer = 1f;
    private bool isDash = false, isAttackEnemy = false, isVulnerable = false;
    private int health;
    private int score;
    private Arrow closestTarget = null;
    private Enemy closestEnemy = null;
    private Boss closestBoss = null;
    private string playerSwipe;
    private int gold = 0;
    private int shield = 0;
    private float shieldTimer = 5;
    private float currentTime = 0;
    private float bossTimer = 3;
    private float currentBossTime;
    private bool isShieldActive = false;
    private bool isShopActive = false;
    private bool isBossBattle = false;
    private Vector3 skillPosition;

    public GameObject shieldButton;
    public GameObject shieldIcon;
    public GameObject shopUI;
    public Text shieldIconTimer;

    // Start is called before the first frame update
    void Start()
    {
        playerDashSpeed = playerSpeed * .50f;
        health = 2;
        GameUI.Instance.displayHealth(health);
        currentTime = shieldTimer;
        currentBossTime = bossTimer;
    }

    void Update()
    {
        // Checks if shop is active
        isShopActive = shopUI.GetComponent<Shop>().getIsShopActive();

        // Checks for health
        if (this.health == 0)
        {
            SceneChanger.Instance.changeScene("GameOver");
        }

        // Checks for shield
        if (isShieldActive == true)
        {
            shieldButton.SetActive(true);
        }

        // If Player owns a shield, button to activate it will appear
        if (shield > 0)
        {
            shieldButton.SetActive(true);
        }

        else
        {
            shieldButton.SetActive(false);
        }

        // Icon and timer to show shield is active
        if (isShieldActive == true)
        {
            isVulnerable = false;
            
            currentTime -= 1 * Time.deltaTime;

            if (currentTime < 0)
            {
                currentTime = 0;
            }

            shieldIconTimer.text = currentTime.ToString();
            
        }

        else
        {
            shieldIconTimer.text = "";
        }

        // Looks for boss
        if (closestBoss != null)
        {
            isBossInRange();
        }

        // Activates boss battle mode
        if (isBossBattle == true && isShopActive == false)
        {
            if (isBossAlive() == true)
            {
                animator.SetBool("isIdle", true);

                findCurrentTarget("boss");

                // Boss will shoot after timer
                currentBossTime -= 1 * Time.deltaTime;

                if (currentBossTime <= 0 && isAttackEnemy == false)
                {
                    closestBoss.shoot();
                    currentBossTime = bossTimer;
                }

                // Player attacks
                if (isAttackEnemy == true && currentTime > 0)
                {
                    playerShoot();
                    isAttackEnemy = false;
                    currentBossTime = bossTimer;
                }

                // Boss attack if Player missed
                if (isVulnerable == true)
                {
                    closestBoss.shoot();
                    isVulnerable = false;
                    currentBossTime = bossTimer;
                }
                
            }

            // When boss is killed
            else
            {
                this.gold += closestBoss.GetComponent<Boss>().giveCoins();
                this.score += closestBoss.GetComponent<Boss>().giveScore();
                GameUI.Instance.displayGold(gold);
                GameUI.Instance.displayScore(score);
                isBossBattle = false;
                GameManager.Instance.removeBoss(closestBoss);
                Spawner.Instance.switchBattleMode(false);
                Spawner.Instance.newSpawnPoint();
                Spawner.Instance.spawnEnemy();
                Spawner.Instance.setNewLevel();
                animator.SetBool("isIdle", false);
            }
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // Movement
        if ((isDash == false || isAttackEnemy == false || isVulnerable == false) && isShopActive == false && isBossBattle == false)
        {
            findCurrentEnemy();
            findCurrentTarget("enemy");
            transform.position += transform.right * playerSpeed * Time.deltaTime;
            animator.SetBool("isVulnerable", false);
            animator.SetBool("isAttack", false);
        }

        // Touch controls will only be available if player isn't vulnerable (swiped wrong) and shop is not active
        if (isVulnerable == false && isShopActive == false)
        {
            TouchControls();
        }
    }

    // Touch controls
    void TouchControls()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                startTouchPos = touch.position;
                endTouchPos = touch.position;
            }

            if (touch.phase == TouchPhase.Ended)
            {
                endTouchPos = touch.position;

                // If touch is swipe
                if (verMove() > swipeLimit && horMove() > swipeLimit)
                {
                    checkSwipe(startTouchPos, endTouchPos);
                    playerAttack();
                }

                // If touch is tap, dash and get bonus score
                else if (verMove() < swipeLimit && horMove() < swipeLimit && startTouchPos == endTouchPos)
                {
                    isDash = true;

                    score += 50;
                    GameUI.Instance.displayScore(score);

                    while (isDash == true)
                    {
                        transform.position += transform.right * playerDashSpeed * Time.deltaTime;

                        dashTimer -= 1 * Time.deltaTime;

                        if (dashTimer <= 0)
                        {
                            isDash = false;
                            dashTimer = 1f;
                        }
                    }
                }
            }
        }
    }

    // Check for swipe information
    void checkSwipe(Vector2 startTouchPos, Vector2 endTouchPos)
    {
        // If swipe if vertical
        if (verMove() > swipeLimit && verMove() > horMove())
        {
            if (endTouchPos.y - startTouchPos.y > 0)
            {
                playerSwipe = "Up";
            }

            else if (endTouchPos.y - startTouchPos.y < 0)
            {
                playerSwipe = "Down";
            }

            startTouchPos = endTouchPos;
        }

        // If swipe is horizontal
        else if (horMove() > swipeLimit && horMove() > verMove())
        {
            if (endTouchPos.x - startTouchPos.x > 0)
            {
                playerSwipe = "Right";
            }

            else if (endTouchPos.x - startTouchPos.x < 0)
            {
                playerSwipe = "Left";
            }

            startTouchPos = endTouchPos;
        }
    }

    // Return values to check swipe orientation
    float verMove()
    {
        return Mathf.Abs(endTouchPos.y - startTouchPos.y);
    }

    float horMove()
    {
        return Mathf.Abs(endTouchPos.x - startTouchPos.x);
    }

    // Look for current target
    Arrow findCurrentTarget(string enemy)
    {
        closestTarget = GameManager.Instance.findClosestArrow(enemy);

        return closestTarget;
    }

    // Look for current boss
    public Boss findCurrentBoss()
    {
        closestBoss = GameManager.Instance.findClosestBoss();
      
        return closestBoss;
    }

    // Compare if player swiped correctly
    void playerAttack()
    {
        // Conditons if arrow is green
        if (closestTarget.colour == "Green")
        {
            if (playerSwipe == closestTarget.orientation)
            {
                if (isBossBattle == true)
                {
                    closestBoss.removeArrow(closestTarget);
                }
                
                else
                {
                    closestEnemy.removeArrow(closestTarget);
                }

                GameManager.Instance.removeArrow(closestTarget.GetComponent<Arrow>());
                Destroy(closestTarget.gameObject);
            }

            else
            {
                isAttackEnemy = false;
                closestTarget.GetComponent<SpriteRenderer>().color = Color.red;
                closestTarget.GetComponent<SpriteRenderer>().sprite = wrong;
            }
        }

        // Conditions if arrow is red
        else if (closestTarget.colour == "Red")
        {
            if ((closestTarget.orientation == "Up" && playerSwipe == "Down") || (closestTarget.orientation == "Down" && playerSwipe == "Up"))
            {
                if (isBossBattle == true)
                {
                    closestBoss.removeArrow(closestTarget);
                }

                else
                {
                    closestEnemy.removeArrow(closestTarget);
                }
                Destroy(closestTarget.gameObject);
            }

            else if ((closestTarget.orientation == "Left" && playerSwipe == "Right") || (closestTarget.orientation == "Right" && playerSwipe == "Left"))
            {
                if (isBossBattle == true)
                {
                    closestBoss.removeArrow(closestTarget);
                }

                else
                {
                    closestEnemy.removeArrow(closestTarget);
                }
                Destroy(closestTarget.gameObject);
            }

            else
            {
                isAttackEnemy = false;
                closestTarget.GetComponent<SpriteRenderer>().color = Color.red;
                closestTarget.GetComponent<SpriteRenderer>().sprite = wrong;
            }
        }

        // For boss
        if (isBossBattle == true)
        {
            if (closestTarget.GetComponent<SpriteRenderer>().sprite != wrong && closestBoss.getArrowCount() == 0)
            {
                isAttackEnemy = true;
                isVulnerable = false;
            }

            else if (closestTarget.GetComponent<SpriteRenderer>().sprite == wrong)
            {
                isVulnerable = true;
                isAttackEnemy = false;
            }
        }
        
        // For enemy
        else
        {
            if (closestTarget.GetComponent<SpriteRenderer>().sprite != wrong && closestEnemy.getArrowCount() == 0)
            {
                isAttackEnemy = true;
                isVulnerable = false;
            }

            else if (closestTarget.GetComponent<SpriteRenderer>().sprite == wrong)
            {
                isVulnerable = true;
                isAttackEnemy = false;
            }
        }
    }

    // Find current enemy
    Enemy findCurrentEnemy()
    {
        closestEnemy = GameManager.Instance.findClosestEnemy();

        return closestEnemy;
    }

    // Results from swipes
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Enemy"))
        {

            // If Player swiped correctly
            if (isAttackEnemy == true)
            {
                animator.SetBool("isAttack", true);
                this.GetComponent<SpriteRenderer>().sprite = punch;
                this.gold += closestEnemy.GetComponent<Enemy>().giveGold();
                this.score += closestEnemy.GetComponent<Enemy>().giveScore();
                GameUI.Instance.displayGold(gold);
                GameUI.Instance.displayScore(score);
                GameManager.Instance.removeEnemy(closestEnemy);
            }

            // If Player swiped wrong
            else
            {
                animator.SetBool("isVulnerable", true);
                this.GetComponent<SpriteRenderer>().sprite = hurt;
                this.health -= 1;
                GameUI.Instance.displayHealth(health);
                closestEnemy.destroyRemainingArrows();
                GameManager.Instance.removeEnemy(closestEnemy);
            }

            isVulnerable = false;
            isAttackEnemy = false;

            // Spawn enemy if boss isn't activated
            if (closestBoss == null && isBossBattle == false && Spawner.Instance.getBossRequiredLevel() > this.score)
            {
                Spawner.Instance.newSpawnPoint();
                Spawner.Instance.spawnEnemy();
            }
        }

        // If hit by claw
        else if (collision.gameObject.CompareTag("Claw"))
        {
            animator.SetBool("isVulnerable", true);
            this.GetComponent<SpriteRenderer>().sprite = hurt;
            this.health -= 1;
            GameUI.Instance.displayHealth(health);
            closestBoss.destroyRemainingArrows();
            Destroy(collision.gameObject);
            StartCoroutine(switchAnimation());
            closestBoss.generateArrows();

            isVulnerable = false;
            isAttackEnemy = false;
        }

    }

    IEnumerator switchAnimation()
    {
        yield return new WaitForSeconds(0.5f);
        animator.SetBool("isVulnerable", false);
    }

    // Player shoot for boss battle
    void playerShoot()
    {
        animator.SetBool("canShoot", true);

        StartCoroutine(shootAnimation());
    }

    IEnumerator shootAnimation()
    {
        skillPosition = new Vector3(skillSpawnPoint.position.x, skillSpawnPoint.position.y, skillSpawnPoint.position.z);
        Instantiate(skill, skillPosition, Quaternion.identity);
        isAttackEnemy = false;
        yield return new WaitForSeconds(0.5f);
        animator.SetBool("canShoot", false);
    }

    // Shield activation
    public void activateShield()
    {
        isShieldActive = true;

        StartCoroutine(shieldActive());
    }

    IEnumerator shieldActive()
    {
        shield -= 1;
        this.GetComponent<BoxCollider2D>().enabled = false;
        shieldIcon.GetComponent<UnityEngine.UI.Image>().enabled = true;
        yield return new WaitForSeconds(5);
        this.GetComponent<BoxCollider2D>().enabled = true;
        shieldIcon.GetComponent<UnityEngine.UI.Image>().enabled = false;
        isShieldActive = false;
        currentTime = shieldTimer;
    }

    // Accessor for score
    public int getScore()
    {
        return this.score;
    }

    // Accessor for health
    public int getHealth()
    {
        return this.health;
    }

    // Receive health from shop
    public void receiveHealth(int num)
    {
        this.health += num;
        GameUI.Instance.displayHealth(this.health);
    }

    // Accessor for shield in player inventory
    public int getShield()
    {
        return this.shield;
    }

    // Receive shield
    public void takeShield(int num)
    {
        this.shield += num;
    }

    // Accessor for gold
    public int getGold()
    {
        return this.gold;
    }

    // Deduct gold
    public void deductGold(int price)
    {
        this.gold -= price;
        GameUI.Instance.displayGold(this.gold);
    }

    // Checks if boss is in range
    private void isBossInRange()
    {
        if (closestBoss.transform.position.y <= bossRange.transform.position.y)
        {
            isBossBattle = true;
        }
    }

    // Checks if boss is alive
    private bool isBossAlive()
    {
        if (closestBoss.getHealth() > 0)
        {
            return true;
        }

        else
        {
            return false;
        }
    }
}
