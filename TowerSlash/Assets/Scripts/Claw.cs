﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Claw : MonoBehaviour
{
    private float speed = 5f;
    private float despawnTime = 5f;
    private float currentTime = 0;

    private Boss owner = null;

    // Start is called before the first frame update
    void Start()
    {
        // Finds its owner
        owner = GameManager.Instance.findClosestBoss();
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += new Vector3(0, -speed, 0) * Time.deltaTime;

        // Destroy self when missed Player (when Player's shield is active)
        currentTime += 1 * Time.deltaTime;

        if(currentTime >= despawnTime)
        {
            owner.destroyRemainingArrows();
            owner.generateArrows();
            Destroy(this.gameObject);
        }
    }

    // Player skill can counter claw
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Skill"))
        {
            Destroy(this.gameObject);
        }
    }
}
