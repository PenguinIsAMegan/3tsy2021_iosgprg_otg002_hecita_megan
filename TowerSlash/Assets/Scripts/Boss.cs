﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    private int difficulty;
    private int gold;
    private int score;
    private int health;

    private List<Arrow> arrowList = new List<Arrow>();
    
    [SerializeField] Animator animator;
    [SerializeField] GameObject claw;
    [SerializeField] Transform clawSpawnPoint;

    // Start is called before the first frame update
    void Start()
    {
        this.health = 1;
        StartCoroutine(spawnArrows());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Gets Difficulty and clamps to max level (3)
    public void getDifficulty(int level)
    {
        this.difficulty = level;

        if (difficulty > 3)
        {
            difficulty = 3;
        }
    }

    // Generates Arrow
    public void generateArrows()
    {
        StartCoroutine(spawnArrows());
    }
    IEnumerator spawnArrows()
    {
        for (int i = 0; i < this.difficulty; i++)
        {
            GameObject newArrow = Spawner.Instance.generateArrow(i);
            arrowList.Add(newArrow.GetComponent<Arrow>());
            yield return new WaitForSeconds(1);
        }
    }

    // Gives rewards
    public int giveCoins()
    {
        return gold = 100 * this.difficulty;
    }

    public int giveScore()
    {
        return score = 100 * this.difficulty;
    }

    // Shoot 
    public void shoot()
    {
        animator.SetBool("canShoot", true);
      
        
        StartCoroutine(switchAnimation());
    }

    IEnumerator switchAnimation()
    {
        yield return new WaitForSeconds(0.5f);
        transform.position = new Vector3(clawSpawnPoint.position.x, clawSpawnPoint.position.y, clawSpawnPoint.position.z);
        Quaternion spawnRotation = Quaternion.Euler(0, 0, 90);
        Instantiate(claw, transform.position, spawnRotation);
        animator.SetBool("canShoot", false);
    }

    // If target is swiped correctly, remove from list
    public void removeArrow(Arrow currentArrow)
    {
        arrowList.Remove(currentArrow.GetComponent<Arrow>());
    }

    // Accessor for current arrow count
    public int getArrowCount()
    {
        return arrowList.Count;
    }

    // If player missed, will destroy remaining arrows
    public void destroyRemainingArrows()
    {
        if (this.arrowList.Count > 0)
        {
            for (int i = 0; i < this.arrowList.Count; i++)
            {
                Destroy(this.arrowList[i].gameObject);
            }
        }
        arrowList = new List<Arrow>();
    }

    // Take damage from player attack
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Skill"))
        {
            this.health -= 1;
            Destroy(collision.gameObject);
        }
    }

    // Accessor for health
    public int getHealth()
    {
        return this.health;
    }

    // Accessor for list of arrows
    public List<Arrow> getArrowList()
    {
        return arrowList;
    }
}
