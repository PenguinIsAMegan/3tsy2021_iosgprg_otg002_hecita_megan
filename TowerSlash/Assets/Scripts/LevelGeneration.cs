﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGeneration : MonoBehaviour
{
    private float platformHeight;

    public GameObject platform;
    public Transform generationPoint;
    public float distanceBetween;

    // Start is called before the first frame update
    void Start()
    {
        platformHeight = platform.GetComponent<BoxCollider2D>().size.y;
    }

    // Update is called once per frame
    void Update()
    {
        // Generates platform and change position
        if(transform.position.y < generationPoint.position.y)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + platformHeight, transform.position.z);

            Instantiate(platform, transform.position, transform.rotation);
        }
    }
}
