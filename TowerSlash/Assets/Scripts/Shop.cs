﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    private int shieldPrice = 100;
    private int healthPrice = 50;

    public bool isEnterShop = false;
    public Player player;
    public GameObject shopButton;
    public GameObject shopMenu;
    public GameObject shieldButton, healthButton;
    public Text warning;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        // Checks if player have enough gold
        if (player.getGold() >= 50)
        {
            shopButton.SetActive(true);
        }
    }

    // Opens shop menu
    public void openShop()
    {
        shopButton.SetActive(false);
        shopMenu.SetActive(true);
        isEnterShop = true;
        
        // Checks if player reached max amount of items
        if (player.getShield() <= 0)
        {
            shieldButton.GetComponent<Button>().interactable = true;
        }

        if (player.getHealth() < 3)
        {
            healthButton.GetComponent<Button>().interactable = true;
        }
    }

    // Buy shield
    public void buyShield()
    {
        // Checks if player can afford
        if (player.getGold() >= shieldPrice)
        {
            player.takeShield(1);
            player.deductGold(shieldPrice);
            shieldButton.GetComponent<Button>().interactable = false;
        }

        else
        {
            StartCoroutine(showWarning("NOT ENOUGH GOLD"));
            shieldButton.GetComponent<Button>().interactable = false;
        }
    }

    // Buy health
    public void buyHealth()
    {
        // Checks if player can afford
        if (player.getGold() >= healthPrice)
        {
            if (player.getHealth() < 3)
            {
                player.receiveHealth(1);
                player.deductGold(healthPrice);
            }

            else if (player.getHealth() > 3)
            {
                healthButton.GetComponent<Button>().interactable = false;
            }
        }

        else
        {
            StartCoroutine(showWarning("NOT ENOUGH GOLD"));
            healthButton.GetComponent<Button>().interactable = false;
        }
    }

    IEnumerator showWarning(string message)
    {
        warning.enabled = true;
        warning.text = message;
      
        yield return new WaitForSeconds(3);
        warning.enabled = false;
    }

    // Exit shop
    public void exitShop()
    {
        shopButton.SetActive(false);
        shopMenu.SetActive(false);
        isEnterShop = false;
    }

    // Accessor if shop is active
    public bool getIsShopActive()
    {
        return isEnterShop;
    }
}
