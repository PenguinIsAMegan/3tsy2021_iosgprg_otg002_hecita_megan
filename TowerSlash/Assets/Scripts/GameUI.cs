﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : Singleton<GameUI>
{
    [SerializeField] int score = 0;
    [SerializeField] int gold = 0;
    [SerializeField] int health;

    public Text healthCtr;
    public Text scoreCtr;
    public Text goldCtr;
    public static GameUI Instance;

    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Display health
    public void displayHealth(int num)
    {
        health = num;
        healthCtr.text = health.ToString();
    }

    // Display score
    public void displayScore(int num)
    {
        score = num;
        scoreCtr.text = score.ToString();
    }

    // Display gold
    public void displayGold(int num)
    {
        gold = num;
        goldCtr.text = gold.ToString();
    }
}
