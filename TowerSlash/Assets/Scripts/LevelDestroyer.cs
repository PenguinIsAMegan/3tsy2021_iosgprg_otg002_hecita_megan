﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDestroyer : MonoBehaviour
{
    public GameObject platformDestructionPoint;

    // Start is called before the first frame update
    void Start()
    {
        platformDestructionPoint = GameObject.Find("DespawnPlatformPoint");
    }

    // Update is called once per frame
    void Update()
    {
        // If passed point, destroy platform
        if (transform.position.y < platformDestructionPoint.transform.position.y)
        {
            Destroy(gameObject);
        }
    }
}
