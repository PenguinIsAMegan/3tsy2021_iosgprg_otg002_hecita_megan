﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Spawner : Singleton<Spawner>
{
    [SerializeField] GameObject VerArrow, HorArrow;
    [SerializeField] GameObject enemy, boss, player;
    [SerializeField] Transform arrowSpawnPoint;
    [SerializeField] Transform enemySpawnPoint;
    [SerializeField] Transform playerPosition;
    [SerializeField] Transform bossSpawnPoint;

    private GameObject newEnemy, newBoss;
    private List<Enemy> enemyList = new List<Enemy>();
    private List<Boss> bossList = new List<Boss>();
    private int enemyDifficulty = 1;
    private int bossDifficulty = 2;
    private int enemySpawnDistance = 0;
    private float enemySpawnRate = 8, enemySpawnTimer;
    private Vector3 currentRotation;
    private int bossRequiredLevel = 1000;
    private bool isBossBattleActive = false;

    public static Spawner Instance;
    public string colour, orientation;
    public bool isFlipped;
    public Transform rangePoint;
    
    

    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        
        spawnEnemy();
    }

    // Update is called once per frame
    void Update()
    {
        // Checks if Player's score reached the required score for boss
        if (player.GetComponent<Player>().getScore() >= bossRequiredLevel)
        {
            isBossBattleActive = true;
            newSpawnPoint();
            spawnBoss();
            player.GetComponent<Player>().findCurrentBoss();
            bossRequiredLevel += 1000;
        }
    }

    // Generate arrows
    public GameObject generateArrow(int space)
    {
        int arrowNum = Random.Range(0, 2);
        GameObject newArrow;

        if (arrowNum == 0)
        {
            this.orientation = "Vertical";
            VerArrow.GetComponent<SpriteRenderer>().color = setColour();
            VerArrow.GetComponent<SpriteRenderer>().flipY = setOrientation();
            VerArrow.GetComponent<Arrow>().setInfo(colour, orientation);

            transform.position = new Vector3(arrowSpawnPoint.position.x, arrowSpawnPoint.position.y + space, arrowSpawnPoint.position.z);

            newArrow = (GameObject)Instantiate(VerArrow, transform.position, Quaternion.identity);
        }

        else
        {
            this.orientation = "Horizontal";
            HorArrow.GetComponent<SpriteRenderer>().color = setColour();
            HorArrow.GetComponent<SpriteRenderer>().flipX = setOrientation();
            HorArrow.GetComponent<Arrow>().setInfo(colour, orientation);

            transform.position = new Vector3(arrowSpawnPoint.position.x, arrowSpawnPoint.position.y + space, arrowSpawnPoint.position.z);

            newArrow = (GameObject)Instantiate(HorArrow, transform.position, Quaternion.identity);
        }

        return newArrow;
    }

    // Set colour
    private Color setColour()
    {
        int colourNum = Random.Range(0, 2);

        if (colourNum == 0)
        {
            colour = "Green";
            return Color.green;
        }

        else         
        {
            colour = "Red";
            return Color.red;
        }
    }

    // Set orientation
    private bool setOrientation()
    {
        int isFlipNum = Random.Range(0, 2);

        if (isFlipNum == 0)
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    // Spawn enemy
    public void spawnEnemy()
    {
        transform.position = new Vector3(enemySpawnPoint.position.x, enemySpawnPoint.position.y, enemySpawnPoint.position.z);
        Quaternion spawnRotation = Quaternion.Euler(0, 0, 90);
        newEnemy = (GameObject)Instantiate(enemy, transform.position, spawnRotation);
        newEnemy.GetComponent<Enemy>().getDifficulty(enemyDifficulty);
        enemyList.Add(newEnemy.GetComponent<Enemy>());
    }

    // Updates spawn position
    public void newSpawnPoint()
    {
        enemySpawnDistance = 8;
        enemySpawnPoint.position = new Vector3(enemySpawnPoint.position.x, enemySpawnPoint.position.y + enemySpawnDistance, enemySpawnPoint.position.z);
    }


    // Accessor for rangePoint
    public Vector3 getRangePoint()
    {
        return rangePoint.position;
    }

    // Accessor for list of enemies
    public List<Enemy> getEnemyList()
    {
        return enemyList;
    }

    // Spawn boss
    public void spawnBoss()
    {
        transform.position = new Vector3(enemySpawnPoint.position.x, enemySpawnPoint.position.y, enemySpawnPoint.position.z);
        Quaternion spawnRotation = Quaternion.Euler(0, 0, 90);
        newBoss = (GameObject)Instantiate(boss, transform.position, spawnRotation);
        newBoss.GetComponent<Boss>().getDifficulty(bossDifficulty);
        bossList.Add(newBoss.GetComponent<Boss>());
    }

    // Accessor for list of boss
    public List<Boss> getBossList()
    {
        return bossList;
    }

    // Switch to boss active
    public void switchBattleMode(bool active)
    {
        isBossBattleActive = active;
    }

    // Gives enemies and boss new difficulty
    public void setNewLevel()
    {
        enemyDifficulty += 1;
        bossDifficulty += 1;
    }

    // Accessor for bossRequiredLevel
    public int getBossRequiredLevel()
    {
        return bossRequiredLevel;
    }
}
