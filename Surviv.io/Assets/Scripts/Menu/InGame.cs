﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGame : MenuBase
{
    public new static InGame Instance;

    [SerializeField] Slider slider;

    public Text clipCtr;
    public Text ownedCtr;
    public Text totalAmmo9mmCtr;
    public Text totalAmmo12GaugeCtr;
    public Text totalAmmo5_56AmmoCtr;

    public Joystick moveJoystick;
    public Joystick aimJoystick;

    public GameObject primaryButton;
    public GameObject secondaryButton;
    public GameObject fireButton;
    public GameObject reloadButton;

    public bool isHold = false;

    Player player;

    protected override void Start()
    {
        base.Start();

        if (Instance == null)
            Instance = this;

        primaryButton.GetComponent<Button>().interactable = false;
        secondaryButton.GetComponent<Button>().interactable = false;
        fireButton.GetComponent<Button>().interactable = false;
        reloadButton.GetComponent<Button>().interactable = false;
    }

    public void Update()
    {
        if (player == null)
            player = GameMngr.Instance.player;

        setHealth();
    }

    // Displays player hp
    public void setHealth()
    {
        slider.value = player.GetComponent<Health>().curHp;
    }

    // Displays amount of current clip and ammo in bag
    public void displayAmmoInfo(int clip, int owned)
    {
        clipCtr.text = clip.ToString();
        ownedCtr.text = owned.ToString();
    }

    // Set interactable buttons
    public void setInteractable(int button, bool status)
    {
        if (button == 1)
            primaryButton.GetComponent<Button>().interactable = status;

        else if (button == 2)
            secondaryButton.GetComponent<Button>().interactable = status;

        fireButton.GetComponent<Button>().interactable = true;
    }

    // Display ammo in bag
    public void displayTotalBullet(Ammo ammo)
    {
        if (ammo.name == "12Gauge")
            totalAmmo12GaugeCtr.text = ammo.getCurrentAmount().ToString();

        else if (ammo.name == "9mm")
            totalAmmo9mmCtr.text = ammo.getCurrentAmount().ToString();

        else
            totalAmmo5_56AmmoCtr.text = ammo.getCurrentAmount().ToString();

        if (ammo.getCurrentAmount() > 0)
            reloadButton.GetComponent<Button>().interactable = true;
    }

    // Fire button
    public void fire()
    {
        player.GetComponent<WeaponManager>().shoot();
    }

    // Reload button
    public void reload()
    {
        player.GetComponent<WeaponManager>().reload();
    }

    // Switch gun button
    public void switchGuns()
    {
        player.GetComponent<WeaponManager>().switchGun();
    }

    // When holding down fire button
    public void onPress()
    {
        if (player.GetComponent<WeaponManager>().getCurrentGun().name == "AutomaticWeapon")
        {
            isHold = true;
            player.GetComponent<WeaponManager>().shoot();
        }
    }

    // When fire button on release
    public void onRelease()
    {
        isHold = false;
    }

    // Accessor for isHold
    public bool getIsHold()
    {
        return isHold;
    }

    // Reset UI on retry
    public void resetUI()
    {
        displayAmmoInfo(0, 0);

        totalAmmo12GaugeCtr.text = "0";
        totalAmmo9mmCtr.text = "0";
        totalAmmo5_56AmmoCtr.text = "0";
        
        primaryButton.GetComponent<Button>().interactable = false;
        secondaryButton.GetComponent<Button>().interactable = false;
        fireButton.GetComponent<Button>().interactable = false;
        reloadButton.GetComponent<Button>().interactable = false;

    }
}
