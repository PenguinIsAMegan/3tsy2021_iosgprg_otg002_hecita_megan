﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MenuType
{
    MainMenu,
    InGame,
    GameOver,
}

public class MenuBase : Singleton<MenuBase>
{
    [SerializeField] MenuType menuType;

    public MenuType getMenuType {  get { return menuType; } }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        MenuMngr.Instance.registerMenu(this);
    }

    // Function to show menu
    public void show()
    {
        if (!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }
    }

    // Function to hide menu
    public void hide()
    {
        if (this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
        }
    }
}
