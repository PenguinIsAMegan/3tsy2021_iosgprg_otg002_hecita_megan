﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MenuBase
{
    // Function to play game
   public void playGame()
    {
        MenuMngr.Instance.showCanvas(MenuType.InGame);
        GameMngr.Instance.startGame();
    }
}
