﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MenuBase
{
    // Function to retry game
   public void retryGame()
    {
        GameMngr.Instance.restartGame();
    }
}
