﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Health : MonoBehaviour
{
    private Unit owner;
    public int curHp;

    private int maxHp;

    public int getMaxHp { get { return maxHp; } }

    // Initialise hp
    public void init(int hp, Unit mOwner)
    {
        this.maxHp = hp;
        this.curHp = maxHp;
        this.owner = mOwner;
    }

    // Take damage, if hp is less or equal to 0, call Death event
    public void takeDamage(int damage)
    {
        this.curHp -= damage;

        if (this.curHp <= 0)
            this.owner.Death.Invoke();
    }
}
