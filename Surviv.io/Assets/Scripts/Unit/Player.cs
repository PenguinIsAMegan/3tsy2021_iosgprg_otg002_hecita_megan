﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Player : Unit
{
    // Joystick reference: https://www.youtube.com/watch?v=bp2PiFC9sSs
    public Joystick moveJoystick;
    public Joystick aimJoystick;

    public WeaponManager weaponManager;

    // Start is called before the first frame update
    public override void Start()
    {
        Death += onDeath;

        VirtualCam.Instance.setFollow(this.transform);
        GameMngr.Instance.player = this;
        this.init(100, 7, this);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameMngr.Instance.player == null)
            GameMngr.Instance.player = this;

        if (!moveJoystick || !aimJoystick)
        {
            moveJoystick = InGame.Instance.moveJoystick;
            aimJoystick = InGame.Instance.aimJoystick;
        }

        playerMovement();

    }

    // Player movement
    void playerMovement()
    {
        // Movement
        float hMove = moveJoystick.Horizontal * speed * Time.deltaTime;
        float vMove = moveJoystick.Vertical * speed * Time.deltaTime;
        this.transform.position += new Vector3(hMove, vMove, 0);

        // Aiming
        if (aimJoystick.Horizontal != 0 || aimJoystick.Vertical != 0)
        {
            float angle = Mathf.Atan2(-aimJoystick.Horizontal, aimJoystick.Vertical) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(0, 0, angle);
        }
    }

    // Player collision
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<GunLoot>())
        {
            var item = collision.gameObject.GetComponent<GunLoot>();
            weaponManager.addGun(item.gunType);
        }

        if(collision.gameObject.GetComponent<AmmoLoot>())
        {
            var item = collision.gameObject.GetComponent<AmmoLoot>();
            weaponManager.addAmmo(item.ammoType);
        }

        if (weaponManager.getClaimStatus() && (!collision.gameObject.CompareTag("Obstacle") && !collision.gameObject.CompareTag("Unit")))
            Destroy(collision.gameObject);
    }

    // When killed, show game over screen, despawn everything, reset inventory and ui
    public override void onDeath()
    {
        GameMngr.Instance.gameOver();
        Spawner.Instance.despawnEverything();
        weaponManager.resetInventory();
        InGame.Instance.resetUI();
    }
}
