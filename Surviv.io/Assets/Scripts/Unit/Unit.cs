﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Unit : MonoBehaviour
{
    protected int speed;
    protected Health health;

    public Action Death;

    // Start is called before the first frame update
    public virtual void Start()
    {
     
    }

    void Update()
    {
        
    }

    // Initialise
    public void init(int nHp, int nSpeed, Unit owner)
    {
        if (this.GetComponent<Health>())
        {
            this.health = this.GetComponent<Health>();
            this.health.init(nHp, owner);
        }

        this.speed = nSpeed;
    }

    // Death
    public virtual void onDeath()
    {

    }
}
