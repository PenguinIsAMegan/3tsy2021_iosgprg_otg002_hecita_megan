﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Unit
{
    [SerializeField] GameObject gunPoint;
    [SerializeField] GameObject bulletDirection;
    [SerializeField] GameObject bullet;
    
    private float range = 5;
    private Gun gun;
    private Animator anim;
    private float fireRate = 0.3f;
    private float currentTimer = 0;

    public bool isObstacleHit = false;
    public Unit currenttarget;

    // Start is called before the first frame update
    public override void Start()
    {
        Death += onDeath;
        
        this.init(100, 5, this);
        anim = this.GetComponent<Animator>();
        initEnemy();
    }

    // Gives weapon
    void initEnemy()
    {
        gun = Spawner.Instance.giveEnemyWeapon(this);
        gunPoint.GetComponent<SpriteRenderer>().sprite = gun.image;
    }

    // Update is called once per frame
    void Update()
    {
        currentTimer += 1 * Time.deltaTime;

        rangeDetection();
    }

    // Detect if hitting something (For Wander.cs, if hit, change target location for Wander)
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
            isObstacleHit = true;

        else if (collision.gameObject.CompareTag("Loot"))
            Destroy(collision.gameObject);
    }

    // Detect nearest target
    void rangeDetection()
    {
        currenttarget = Spawner.Instance.findNearestUnit(this);

        if (Vector2.Distance(this.transform.position, currenttarget.transform.position) <= range)
            anim.SetBool("isInRange", true);

        else
            anim.SetBool("isInRange", false);
    }

    // Accessor for Enemy speed
    public float getSpeed()
    {
        return speed;
    }

    // Idle timer before wandering again
    IEnumerator idleTimer()
    {
        yield return new WaitForSeconds(3f);
        anim.SetBool("isReach", false);
    }

    // Triggers idleTimer
    public void triggerTimer()
    {
        anim.SetBool("isReach", true);
        StartCoroutine(idleTimer());
    }

    // Shoot
    public void shoot()
    {
        if (gun.getCurrentClip() <= 0)
            StartCoroutine(reloadTimer());

        if (currentTimer >= fireRate)
        {
            Vector2 difference = bulletDirection.transform.position - gunPoint.transform.position;

            gun.shoot(difference, gunPoint, bullet);

            currentTimer = 0;
        }
    }

    // Reload
    public void reload()
    {
        gun.setCurrentClip(gun.clipSize);
    }

    // Reload timer
    IEnumerator reloadTimer()
    {
        yield return new WaitForSeconds(2f);
        reload();
    }

    // Drops loot and replace Enemy unit on death
    public override void onDeath()
    {
        Spawner.Instance.replaceUnit(this);
    }
}
