﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBaseFSM : StateMachineBehaviour
{
    public float rotSpeed = 1.0f;
    public GameObject ai;
    public Unit otherUnits;
    public MeshCollider box;

    public float posX;
    public float posY;
    public float range = 5f;

    public Vector3 target;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ai = animator.gameObject;
        box = Spawner.Instance.getbox();
        randomPoint();
    }

    // Gets random point within box
    public virtual void randomPoint()
    {
        posX = Random.Range(box.bounds.min.x, box.bounds.max.x);
        posY = Random.Range(box.bounds.min.y, box.bounds.max.y);

        target = new Vector3(posX, posY, 0);
        Vector3 direction = target - ai.transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        ai.transform.eulerAngles = new Vector3(0, 0, angle);
        ai.GetComponent<Enemy>().isObstacleHit = false;
    }
}
