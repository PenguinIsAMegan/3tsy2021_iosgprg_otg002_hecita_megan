﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wander : EnemyBaseFSM
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // If target reach, idle before moving again
        if (ai.transform.position == target)
            ai.GetComponent<Enemy>().triggerTimer();

        // If hit something, change direction
        if (ai.GetComponent<Enemy>().isObstacleHit)
            randomPoint();

        // Move to target
        ai.transform.position = Vector3.MoveTowards(ai.transform.position, target, ai.GetComponent<Enemy>().getSpeed() * Time.deltaTime);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
