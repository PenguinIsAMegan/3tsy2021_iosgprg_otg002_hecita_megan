﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    [SerializeField] Gun[] gunList;
    [SerializeField] Gun currentGun;
    [SerializeField] List<Ammo> ammoList = new List<Ammo>();
    [SerializeField] GameObject gunPoint;
    [SerializeField] GameObject bulletDirection;
    [SerializeField] GameObject bullet;

    private bool hasClaimed;
    private int index;
    private int currentAmmo;
    private float fireRate = 0.3f;
    private float currentTimer = 0;

    public void addGun(Gun gun)
    {
        // If slot is empty, take gun
        if ((gun.name == "Shotgun" || gun.name == "AutomaticWeapon") && !gunList[0])
        {
            gunList[0] = gun;
            hasClaimed = true;
        }

        else if ((gun.name == "Pistol") && !gunList[1])
        {
            gunList[1] = gun;
            hasClaimed = true;
        }

        else
            hasClaimed = false;

        // If no current gun, set new gun as current gun
        if (!currentGun)
        {
            currentGun = gun;
            renderGun();
        }

        // Enable buttons for gun switching
        if (gunList[0] && gunList[1])
        {
            if (currentGun.name == "Shotgun" || currentGun.name == "AutomaticWeapon")
                InGame.Instance.setInteractable(2, true);

            else
                InGame.Instance.setInteractable(1, true);
            
        }
        
        InGame.Instance.setInteractable(3, true);
        displayInfo();
    }

    // Accessor for hasClaimed
    public bool getClaimStatus()
    {
        return hasClaimed;
    }

    // Display info
    private void displayInfo()
    {
        if (currentGun && !checkForBullet(out index))
            InGame.Instance.displayAmmoInfo(currentGun.getCurrentClip(), 0);

        else if (currentGun && checkForBullet(out index))
            InGame.Instance.displayAmmoInfo(currentGun.getCurrentClip(), ammoList[index].getCurrentAmount());
    }

    // Check if bullet is owned
    private bool checkForBullet(out int index)
    {
        bool isOwned = false;

        for (int i = 0; i < ammoList.Count; i++)
        {
            if (currentGun.ammoType == ammoList[i].name)
            {
                isOwned = true;
                index = i;
                return isOwned;
            }
        }

        index = 3;
        return isOwned;
    }
    
    // Add ammo
    public void addAmmo(Ammo ammo)
    {
        bool isOwned = false;

        for (int i = 0; i < ammoList.Count; i++)
        {
            // If ammo is owned, add looted ammo to current ammo count
            if (ammo.name == ammoList[i].name)
            {
                int total = ammoList[i].getCurrentAmount() + ammoList[i].maxAmount;
                ammoList[i].setCurrentAmount(total);
                isOwned = true;
                break;
            }
        }

        // If ammo not owned, add ammo to list
        if (!isOwned)
        {
            ammoList.Add(ammo);
        }

        hasClaimed = true;
        displayInfo();
        InGame.Instance.displayTotalBullet(ammo);
    }

    // Switch gun
    public void switchGun()
    {
        if ((currentGun.name == "Shotgun" || currentGun.name == "AutomaticWeapon") && gunList[1])
        {
            currentGun = gunList[1];
            InGame.Instance.setInteractable(2, false);
            InGame.Instance.setInteractable(1, true);
        }


        else if (currentGun.name == "Pistol" && gunList[0])
        {
            currentGun = gunList[0];
            InGame.Instance.setInteractable(1, false);
            InGame.Instance.setInteractable(2, true);
        }

        renderGun();
        displayInfo();
    }

    // Shoot gun
    public void shoot()
    {
        if(currentGun && currentTimer >= fireRate)
        {
            checkForBullet(out index);

            Vector2 difference = bulletDirection.transform.position - gunPoint.transform.position;

            currentGun.shoot(difference, gunPoint, bullet);
            currentTimer = 0;

            displayInfo();
        }
    }

    // Reload gun
    public void reload()
    {
        if (currentGun)
        {
            // If there's ammo in bag, reload
            if (checkForBullet(out index) && index < 3)
                currentGun.reload(ammoList[index]);
        }

        displayInfo();
        InGame.Instance.displayTotalBullet(ammoList[index]);
    }

    // Render gun
    public void renderGun()
    {
        gunPoint.GetComponent<SpriteRenderer>().sprite = currentGun.image;
    }

    // Clear list 
    private void OnApplicationQuit()
    {
        Array.Clear(gunList, 0, gunList.Length);
        ammoList.Clear();
    }

    void Update()
    {
        currentTimer += 1 * Time.deltaTime;

        // Fire function for Automatic Weapon
        if (currentGun && InGame.Instance.getIsHold() && currentGun.name == "AutomaticWeapon")
        {
            if (currentTimer >= fireRate)
                shoot();
        }
    }

    // Reset inventory for retry purpose
    public void resetInventory()
    {
        Array.Clear(gunList, 0, gunList.Length);
        ammoList.Clear();
    }

    // Accessor for currentGun
    public Gun getCurrentGun()
    {
        return currentGun;
    }
}
