﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : EnemyBaseFSM
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Gets current position of this target unit
        Vector3 enemyPos = ai.GetComponent<Enemy>().currenttarget.transform.position;

        // Rotate to look at target
        Vector3 direction = enemyPos - ai.transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        ai.transform.eulerAngles = new Vector3(0, 0, angle);
        ai.GetComponent<Enemy>().isObstacleHit = false;

        // This ai shoots
        ai.GetComponent<Enemy>().shoot();
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
