﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMngr : Singleton<GameMngr>
{
    public Player player { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(AsyncLoadScene("Menu", onMenuLoaded));
        StartCoroutine(AsyncLoadScene("Game"));
    }

    void onMenuLoaded()
    {
        MenuMngr.Instance.showCanvas(MenuType.MainMenu);
    }
    IEnumerator AsyncLoadScene(string name, Action onCallback = null)
    {
        AsyncOperation asyncLoadScene = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);

        while (!asyncLoadScene.isDone)
        {
            yield return null;
        }

        if (onCallback != null)
            onCallback.Invoke();
    }

    // Spawns game (Player, loot, enemies...)
    public void startGame()
    {
        Spawner.Instance.initSpawn();
    }

    // Shows game over canvas
    public void gameOver()
    {
        MenuMngr.Instance.showCanvas(MenuType.GameOver);
    }

    // Restarts game
    public void restartGame()
    {
        startGame();
        MenuMngr.Instance.showCanvas(MenuType.InGame);
    }
}
