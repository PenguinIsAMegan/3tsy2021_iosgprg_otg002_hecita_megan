﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMngr : Singleton<MenuMngr>
{
    [SerializeField] List<MenuBase> menuCanvasList = new List<MenuBase>();

    public void registerMenu(MenuBase menuCanvas)
    {
        menuCanvasList.Add(menuCanvas);
        menuCanvas.hide();
    }

    public void hideAll()
    {
        foreach (MenuBase menuCanvas in menuCanvasList)
        {
            menuCanvas.hide();
        }
    }

    public void showCanvas(MenuType menuType)
    {
        hideAll();

        foreach (MenuBase menuCanvas in menuCanvasList)
        {
            if(menuCanvas.getMenuType == menuType)
            {
                menuCanvas.show();
                break;
            }
        }
    }
}
