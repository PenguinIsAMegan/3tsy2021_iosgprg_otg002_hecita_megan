﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunLoot : MonoBehaviour
{
    public Gun gunType;

    public new string name;

    // Set current clip for ui purposes
    void Start()
    {
        gunType.setCurrentClip(gunType.clipSize);
    }
}
