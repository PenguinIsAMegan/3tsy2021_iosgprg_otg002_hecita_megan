﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoLoot : MonoBehaviour
{
    public Ammo ammoType;

    // Set current amount for ui purposes
    void Start()
    {
        ammoType.setCurrentAmount(ammoType.maxAmount);
    }
}
