﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ammo", menuName = "Ammo")]
public class Ammo : ScriptableObject
{
    public new string name;
    public int maxAmount;
    protected int currentAmount;
    public int damage;

    // Set current amount of Ammo
    public void setCurrentAmount(int amount)
    {
        currentAmount = amount;
    }

    // Accessor for currentAmount
    public int getCurrentAmount()
    {
        return currentAmount;
    }
}
