﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

[CreateAssetMenu(fileName = "New Pistol", menuName = "Gun/Pistol")]
public class PistolData : Gun
{
   
    public override void shoot(Vector2 difference, GameObject spawnPoint, GameObject prefab)
    {
        // If there's ammo availabe, shoot
        if (this.getCurrentClip() > 0)
        {
            int total = this.getCurrentClip() - 1;
            this.setCurrentClip(total);
            float distance = difference.magnitude;
            float rotation = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            Vector2 direction = difference / distance;
            direction.Normalize();
            GameObject bullet = Instantiate(prefab) as GameObject;
            bullet.transform.SetParent(spawnPoint.transform);
            bullet.GetComponent<Bullet>().setDamage(this.bullet.damage);
            bullet.transform.position = spawnPoint.transform.position;
            bullet.transform.rotation = Quaternion.Euler(0, 0, rotation);
            bullet.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;
        }
    }

    public override void reload(Ammo ammo)
    {
        int total = 0;

        // If there is extra ammo and the amount is greater than clip size, reload appropriately
        if (ammo.getCurrentAmount() > 0 && ammo.getCurrentAmount() >= this.clipSize && this.currentClip <= this.clipSize)
        {
            for (int i = this.getCurrentClip(); i < this.clipSize; i++)
            {
                total++;
            }

            int add = this.getCurrentClip() + total;
            this.setCurrentClip(add);
            int remove = ammo.getCurrentAmount() - total;
            ammo.setCurrentAmount(remove);
        }

        // If there's extra ammo and it's lesser than clip size, reload all ammo
        else if (ammo.getCurrentAmount() > 0 && ammo.getCurrentAmount() < this.clipSize && this.currentClip <= this.clipSize) 
        {
            total = this.currentClip + ammo.getCurrentAmount();
            this.setCurrentClip(total);
            ammo.setCurrentAmount(0);
        }
    }
}
