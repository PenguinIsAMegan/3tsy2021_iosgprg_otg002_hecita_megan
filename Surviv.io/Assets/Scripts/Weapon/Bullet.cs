﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private int damage;
    private float destroyTimer = 5f;
    private float currentTime;

    void Update()
    {
        currentTime += 1 * Time.deltaTime;

        // Self destruct when Bullet didn't hit anyone
        if (currentTime >= destroyTimer)
        {
            Destroy(this.gameObject);
            currentTime = 0;
        }
    }

    // Set bullet damage
    public void setDamage(int mDamage)
    {
        this.damage = mDamage;
    }

    // Bullet collision
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Unit"))
        {
            collision.gameObject.GetComponent<Health>().takeDamage(this.damage);
            Destroy(this.gameObject);
        }

        else if (collision.gameObject.CompareTag("Projectile"))
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<BoxCollider2D>(), this.gameObject.GetComponent<BoxCollider2D>());
        
        else
            Destroy(this.gameObject);
        
    }
}
