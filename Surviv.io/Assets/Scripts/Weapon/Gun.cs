﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Gun : ScriptableObject
{
    public new string name;
    public string ammoType;
    public int clipSize;
    protected int currentClip;
    public Sprite image;
    public Ammo bullet;
    protected float bulletSpeed = 30.0f;

    // Set current clip
    public void setCurrentClip(int amount)
    {
        currentClip = amount;
    }

    // Accessor for current clip
    public virtual int getCurrentClip()
    {
        return this.currentClip;
    }

    // Shoot
    public abstract void shoot(Vector2 difference, GameObject spawnPoint, GameObject prefab);

    // Reload
    public abstract void reload(Ammo ammo);
}
