﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualCam : Singleton<VirtualCam>
{
    public new static VirtualCam Instance;

    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
            Instance = this;
    }

    public void setFollow(Transform player)
    {
        var cam = this.GetComponent<CinemachineVirtualCamera>();
        cam.Follow = player;
    }
}
