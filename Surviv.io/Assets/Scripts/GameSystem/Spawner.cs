﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : Singleton<Spawner>
{
    public new static Spawner Instance;

    public GameObject player;
    public GameObject boundingBox;
    public GameObject enemy;

    [SerializeField] Gun[] gunList;
    [SerializeField] Ammo[] ammoList;
    [SerializeField] GunLoot[] gunLootList;
    [SerializeField] List<Unit> unitList = new List<Unit>();
    [SerializeField] AmmoLoot[] ammoLootList;

    private int itemSpawnNum = 15;
    private int enemySpawnNum = 10;

    MeshCollider box;
    Unit closestUnit;

    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null)
            Instance = this;

        box = boundingBox.GetComponent<MeshCollider>();
    }

    // Spawn starting objects
    public void initSpawn()
    {
        spawn(player);

        for (int i = 0; i < itemSpawnNum; i++)
        {
            spawn(gunLootList[Random.Range(0,3)].gameObject);
            spawn(ammoLootList[Random.Range(0, 3)].gameObject);
        }

        for (int i = 0; i < enemySpawnNum; i++)
            spawn(enemy);
    }

    // Spawn objects
    public void spawn(GameObject prefab)
    {
        
        float posX = Random.Range(box.bounds.min.x, box.bounds.max.x);
        float posY = Random.Range(box.bounds.min.y, box.bounds.max.y);

        Vector2 pos = new Vector2(posX, posY);
        GameObject newSpawn = (GameObject)Instantiate(prefab, pos, Quaternion.identity);

        if (newSpawn.GetComponent<Unit>())
            unitList.Add(newSpawn.GetComponent<Unit>());

        newSpawn.transform.SetParent(this.transform);
    }

    // Give enemy a weapon
    public Gun giveEnemyWeapon(Enemy enemy)
    {
        int randNum = Random.Range(0, 3);

        return gunList[randNum];
    }

    // Find nearest unit for enemy
    public Unit findNearestUnit(Unit seeker)
    {
        float distanceToClosestUnit = Mathf.Infinity;
        
        foreach (Unit spawnedUnit in unitList)
        {
            float distanceToUnit = (spawnedUnit.transform.position - seeker.transform.position).sqrMagnitude;

            if (distanceToUnit < distanceToClosestUnit && spawnedUnit != seeker)
            {
                distanceToClosestUnit = distanceToUnit;
                closestUnit = spawnedUnit;
            }
        }

        return closestUnit;
    }

    // Setting bounds
    public MeshCollider getbox()
    {
        return box;
    }

    // Replace a unit if current unit count is less than 16
    public void replaceUnit(Unit unit)
    {
        unitList.Remove(unit);

        if (unitList.Count < 16)
            spawn(enemy);

        Destroy(unit.gameObject);
    }

    // Despawn everything for retry purpose
    public void despawnEverything()
    {
        unitList.Clear();

        foreach (Transform child in this.transform)
            GameObject.Destroy(child.gameObject);
    }
}
