﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : Singleton<UI>
{
    public new static UI Instance;
    public Text clipCtr;
    public Text ownedCtr;
    public Text totalAmmo9mmCtr;
    public Text totalAmmo12GaugeCtr;
    public Text totalAmmo5_56AmmoCtr;
    public GameObject primaryButton;
    public GameObject secondaryButton;
    public GameObject fireButton;
    public GameObject reloadButton;

    void Start()
    {
        if (Instance == null)
            Instance = this;

        //primaryButton.GetComponent<Button>().interactable = false;
        //secondaryButton.GetComponent<Button>().interactable = false;
        //fireButton.GetComponent<Button>().interactable = false;
        //reloadButton.GetComponent<Button>().interactable = false;
    }

    public void displayAmmoInfo(int clip, int owned)
    {
        clipCtr.text = clip.ToString();
        ownedCtr.text = owned.ToString();
    }

    public void setInteractable(int button, bool status)
    {
        if (button == 1)
            primaryButton.GetComponent<Button>().interactable = status;

        else if (button == 2)
            secondaryButton.GetComponent<Button>().interactable = status;

        fireButton.GetComponent<Button>().interactable = true;
    }

    public void displayTotalBullet(Ammo ammo)
    {
        if (ammo.name == "12Gauge")
            totalAmmo12GaugeCtr.text = ammo.getCurrentAmount().ToString();

        else if (ammo.name == "9mm")
            totalAmmo9mmCtr.text = ammo.getCurrentAmount().ToString();

        else
            totalAmmo5_56AmmoCtr.text = ammo.getCurrentAmount().ToString();

        if (ammo.getCurrentAmount() > 0)
            reloadButton.GetComponent<Button>().interactable = true;

    }
}
